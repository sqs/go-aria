// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package context

import (
	"bitbucket.org/osddk/go-aria/environment"
	"bitbucket.org/osddk/go-aria/session"
	"net/http"
)

type Context struct {
	Env      *environment.Environment
	Request  *http.Request
	Response http.ResponseWriter
	Session  *session.Session
}

func NewContext(env *environment.Environment, w http.ResponseWriter, r *http.Request) *Context {
	return &Context{
		Env:      env,
		Request:  r,
		Response: w,
		Session:  nil,
	}
}

func (ctx *Context) HttpClient() *http.Client {
	if fn, ok := ctx.Env.Var["httpclient"].(func(*Context) *http.Client); ok {
		return fn(ctx)
	}
	return &http.Client{}
}
