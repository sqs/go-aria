// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package mux

import (
	"net/http"
	"path"
)

// Return the canonical path for p, eliminating . and .. elements.
// See: net/http/server.go
func cleanPath(p string) string {
	if p == "" {
		return "/"
	}
	if p[0] != '/' {
		p = "/" + p
	}
	np := path.Clean(p)
	// path.Clean removes trailing slash except for root;
	// put the trailing slash back if necessary.
	if p[len(p)-1] == '/' && np != "/" {
		np += "/"
	}
	return np
}

// Mux is an HTTP request multiplexer.
type Mux struct {
	r               []Route
	NotFoundHandler http.Handler
}

// NewMux returns a new Mux.
func NewMux() *Mux {
	return &Mux{NotFoundHandler: http.NotFoundHandler()}
}

// DefaultMux is the default mux used.
var DefaultMux = NewMux()

// AddRoute adds a route to the default mux.
func AddRoute(r Route) {
	DefaultMux.AddRoute(r)
}

// handler locates a matching router for the request and calls the associated
// router handler.
func (mux *Mux) handler(r *http.Request) (h http.Handler) {
	for _, route := range mux.r {
		if h = route.Match(r); h != nil {
			break
		}
	}
	if h == nil {
		h = mux.NotFoundHandler
	}
	return h
}

// AddRoute adds a route to the specified mux.
func (mux *Mux) AddRoute(r Route) {
	mux.r = append(mux.r, r)
}

// ServeHTTP takes an incoming request, sanitizes the request URL and dispatches
// the request to a matching router's handler.
func (mux *Mux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Clean path to canonical form and redirect.
	if p := cleanPath(r.URL.Path); p != r.URL.Path {
		h := http.RedirectHandler(p, http.StatusMovedPermanently)
		h.ServeHTTP(w, r)
		return
	}

	// Serve the request.
	h := mux.handler(r)
	h.ServeHTTP(w, r)
}
