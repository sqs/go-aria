// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package mux

import (
	"net/http"
)

type LiteralRoute struct {
	path    string
	handler http.HandlerFunc
}

func NewLiteralRoute(p string, fn http.HandlerFunc) *LiteralRoute {
	return &LiteralRoute{path: p, handler: http.HandlerFunc(fn)}
}

func (route *LiteralRoute) Match(r *http.Request) (h http.Handler) {
	if route.path == r.URL.Path {
		h = route.handler
	}
	return h
}
