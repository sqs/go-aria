// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package mux

import (
	"net/http"
)

type MethodRoute struct {
	path          string
	methodHandler map[string]http.HandlerFunc
	header        map[string]string
}

// NewMethodRoute returns a new MethodRoute.
func NewMethodRoute(p string) *MethodRoute {
	return &MethodRoute{
		path:          p,
		methodHandler: make(map[string]http.HandlerFunc),
		header:        make(map[string]string),
	}
}

// HandleFunc sets the specified method to call the HandlerFunc specified.
func (route *MethodRoute) HandleFunc(method string, fn http.HandlerFunc) {
	route.methodHandler[method] = fn

	// Add method to CORS header.
	if m, ok := route.header["Access-Control-Allow-Methods"]; ok {
		route.header["Access-Control-Allow-Methods"] = m + ", " + method
	} else {
		route.header["Access-Control-Allow-Methods"] = method
	}
}

// Match finds and returns a matching http.Handler if any.
func (route *MethodRoute) Match(r *http.Request) http.Handler {
	if route.path != r.URL.Path {
		return nil
	}
	h, ok := route.methodHandler[r.Method]
	if !ok && r.Method == "OPTIONS" {
		h = http.HandlerFunc(route.Options)
	}
	return h
}

// Options responds to an options request with proper CORS headers.
func (route *MethodRoute) Options(w http.ResponseWriter, r *http.Request) {
	for h, v := range route.header {
		w.Header().Add(h, v)
	}
}
