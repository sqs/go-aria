// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package session

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAttachCookie(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s, _ := NewSession(r, "test")
		s.AttachCookie(w)
	}))
	defer ts.Close()

	id := "abcdefghijklmnopqrstuvwxyz1234567890"

	client := &http.Client{}
	req, err := http.NewRequest("GET", ts.URL, nil)
	if err != nil {
		t.Error("Failed creating new request:", err)
	}
	req.Header.Add("x-test-"+SessionName, id)
	resp, err := client.Do(req)
	if err != nil {
		t.Error("Failed request:", err)
	}
	defer resp.Body.Close()

	var respid string
	for _, c := range resp.Cookies() {
		if c.Name == "test-"+SessionName {
			respid = c.Value
			break
		}
	}

	if respid != id {
		t.Errorf("Response cookie id does not match request id: Expected \"%s\", got \"%s\"", id, respid)
	}
}

func TestAttachHeader(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s, _ := NewSession(r, "test")
		s.AttachHeader(w)
	}))
	defer ts.Close()

	id := "abcdefghijklmnopqrstuvwxyz1234567890"

	client := &http.Client{}
	req, err := http.NewRequest("GET", ts.URL, nil)
	if err != nil {
		t.Error("Failed creating new request:", err)
	}
	req.Header.Add("x-test-"+SessionName, id)
	resp, err := client.Do(req)
	if err != nil {
		t.Error("Failed request:", err)
	}
	defer resp.Body.Close()

	h := resp.Header.Get("x-test-" + SessionName)
	if h != id {
		t.Errorf("Response header id does not match request id: Expected \"%s\", got \"%s\"", id, h)
	}
}
