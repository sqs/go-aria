// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package storage

import ()

type VolatileStorage struct {
	data interface{}
}

func NewVolatileStorage() *VolatileStorage {
	return &VolatileStorage{data: nil}
}

func (s *VolatileStorage) Clear() {
	s.data = nil
}

func (s *VolatileStorage) IsEmpty() bool {
	return s.data == nil
}

func (s *VolatileStorage) Read() interface{} {
	return s.data
}

func (s *VolatileStorage) Write(v interface{}) {
	s.data = v
}
